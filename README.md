# Centre of Excellence and Technological Innovation in Bioimaging | Health Regional Ministry of Valencia #

[http://ceib.san.gva.es/](http://ceib.san.gva.es/)

## Subdirección General de Sistemas de Información para la Salud ##

### Conselleria de Sanitat Universal i Salut Pública ###

[http://www.san.gva.es/](http://www.san.gva.es/)

**Team leader**: María de la Iglesia Vayá | maigva AT gmail DOT com

Ángel Fernández-Cañada Vilata | anferca83v2 AT gmail DOT com

Jorge Isnardo Altamirano | jisnardo88 AT gmail DOT com