**Instructions**

---

This script will generate text/ascii tables of FreeSurfer:

- aseg stats data, aseg.stats ([asegstats2table](https://surfer.nmr.mgh.harvard.edu/fswiki/asegstats2table))
- parcellation stats data, ?h.aparc.stats ([aparcstats2table](https://surfer.nmr.mgh.harvard.edu/fswiki/aparcstats2table))

---

**Example**

ceib-cs/freesurfer ls

    README.md  stats2table.sh  test-subjects

---

ls test-subjects/

    EP-0001  EP-0004  EP-0007  EP-0010  EP-0013  EP-0016  EP-0019  EP-0022  EP-0025  EP-0028  EP-0031  EP-0034
    EP-0002  EP-0005  EP-0008  EP-0011  EP-0014  EP-0017  EP-0020  EP-0023  EP-0026  EP-0029  EP-0032  EP-0035
    EP-0003  EP-0006  EP-0009  EP-0012  EP-0015  EP-0018  EP-0021  EP-0024  EP-0027  EP-0030  EP-0033

ls test-subjects/EP-0001/stats/

    aseg.stats                 lh.aparc.stats      lh.curv.stats               rh.aparc.a2009s.stats      rh.BA.stats         rh.entorhinal_exvivo.stats
    lh.aparc.a2009s.stats      lh.BA.stats         lh.entorhinal_exvivo.stats  rh.aparc.DKTatlas40.stats  rh.BA.thresh.stats  rh.w-g.pct.stats
    lh.aparc.DKTatlas40.stats  lh.BA.thresh.stats  lh.w-g.pct.stats            rh.aparc.stats             rh.curv.stats       wmparc.stats

---

**1) export FREESURFER_HOME**
    
    export FREESURFER_HOME=/opt/software/freesurfer

**2) test stats2table.sh**
    
    bash stats2table.sh
        
        export SUBJECTS_DIR=/...
        
        source $FREESURFER_HOME/SetUpFreeSurfer.sh
        
        bash stats2table.sh $SUBJECTS_DIR OS delimiter
        
        OS        : linux / mac
        delimiter : csv   / tsv

**3) export SUBJECTS_DIR**
    
    export SUBJECTS_DIR=`pwd`/test-subjects/

**4) configure FREESURFER_HOME**
    
    source $FREESURFER_HOME/SetUpFreeSurfer.sh
        
        -------- freesurfer-centos6_x86_64-stable-pub-v5.3.0 --------
        Setting up environment for FreeSurfer/FS-FAST (and FSL)
        FREESURFER_HOME   /opt/software/freesurfer
        FSFAST_HOME       /opt/software/freesurfer/fsfast
        FSF_OUTPUT_FORMAT nii.gz
        SUBJECTS_DIR      /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        MNI_DIR           /opt/software/freesurfer/mni
        FSL_DIR           /usr/share/fsl/5.0

**5) run stats2table.sh**
    
    bash stats2table.sh $SUBJECTS_DIR linux csv
        
        Subjects
        
        EP-0001
        EP-0002
        EP-0003
        EP-0004
        EP-0005
        EP-0006
        EP-0007
        EP-0008
        EP-0009
        EP-0010
        EP-0011
        EP-0012
        EP-0013
        EP-0014
        EP-0015
        EP-0016
        EP-0017
        EP-0018
        EP-0019
        EP-0020
        EP-0021
        EP-0022
        EP-0023
        EP-0024
        EP-0025
        EP-0026
        EP-0027
        EP-0028
        EP-0029
        EP-0030
        EP-0031
        EP-0032
        EP-0033
        EP-0034
        EP-0035
        
        statsfiles: aseg wmparc
        
        asegmeasures: volume Area_mm2 nvoxels nvertices mean std max
        
        statsfile: aseg
        
        asegmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aseg-volume.csv
        
        asegmeasure: Area_mm2
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aseg-Area_mm2.csv
        
        asegmeasure: nvoxels
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aseg-nvoxels.csv
        
        asegmeasure: nvertices
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aseg-nvertices.csv
        
        asegmeasure: mean
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aseg-mean.csv
        
        asegmeasure: std
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aseg-std.csv
        
        asegmeasure: max
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aseg-max.csv
        
        statsfile: wmparc
        
        asegmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/wmparc-volume.csv
        
        asegmeasure: Area_mm2
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/wmparc-Area_mm2.csv
        
        asegmeasure: nvoxels
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/wmparc-nvoxels.csv
        
        asegmeasure: nvertices
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/wmparc-nvertices.csv
        
        asegmeasure: mean
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/wmparc-mean.csv
        
        asegmeasure: std
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/wmparc-std.csv
        
        asegmeasure: max
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/wmparc-max.csv
        
        statsfiles: aparc.a2009s aparc.DKTatlas40 aparc BA BA.thresh entorhinal_exvivo
        
        hemispheres: lh rh
        
        aparcmeasures: area volume thickness thicknessstd meancurv gauscurv foldind curvind
        
        statsfile: aparc.a2009s
        
        hemisphere: lh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-lh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-lh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-lh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-lh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-lh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-lh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-lh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-lh-curvind.csv
        
        hemisphere: rh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-rh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-rh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-rh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-rh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-rh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-rh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-rh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.a2009s-rh-curvind.csv
        
        statsfile: aparc.DKTatlas40
        
        hemisphere: lh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-lh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-lh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-lh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-lh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-lh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-lh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-lh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-lh-curvind.csv
        
        hemisphere: rh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-rh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-rh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-rh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-rh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-rh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-rh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-rh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc.DKTatlas40-rh-curvind.csv
        
        statsfile: aparc
        
        hemisphere: lh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-lh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-lh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-lh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-lh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-lh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-lh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-lh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-lh-curvind.csv
        
        hemisphere: rh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-rh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-rh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-rh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-rh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-rh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-rh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-rh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/aparc-rh-curvind.csv
        
        statsfile: BA
        
        hemisphere: lh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-lh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-lh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-lh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-lh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-lh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-lh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-lh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-lh-curvind.csv
        
        hemisphere: rh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-rh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-rh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-rh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-rh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-rh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-rh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-rh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA-rh-curvind.csv
        
        statsfile: BA.thresh
        
        hemisphere: lh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-lh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-lh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-lh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-lh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-lh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-lh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-lh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-lh-curvind.csv
        
        hemisphere: rh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-rh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-rh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-rh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-rh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-rh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-rh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-rh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/BA.thresh-rh-curvind.csv
        
        statsfile: entorhinal_exvivo
        
        hemisphere: lh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-lh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-lh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-lh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-lh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-lh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-lh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-lh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-lh-curvind.csv
        
        hemisphere: rh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-rh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-rh-volume.csv
        
        aparcmeasure: thickness
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-rh-thickness.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-rh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-rh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-rh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-rh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/entorhinal_exvivo-rh-curvind.csv
        
        statsfile: w-g.pct
        
        asegmeasures: volume Area_mm2 nvoxels nvertices mean std max snr
        
        asegmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-volume.csv
        
        asegmeasure: Area_mm2
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-Area_mm2.csv
        
        asegmeasure: nvoxels
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-nvoxels.csv
        
        asegmeasure: nvertices
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-nvertices.csv
        
        asegmeasure: mean
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-mean.csv
        
        asegmeasure: std
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-std.csv
        
        asegmeasure: max
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-max.csv
        
        asegmeasure: snr
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-snr.csv
        
        asegmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-volume.csv
        
        asegmeasure: Area_mm2
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-Area_mm2.csv
        
        asegmeasure: nvoxels
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-nvoxels.csv
        
        asegmeasure: nvertices
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-nvertices.csv
        
        asegmeasure: mean
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-mean.csv
        
        asegmeasure: std
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-std.csv
        
        asegmeasure: max
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-max.csv
        
        asegmeasure: snr
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-snr.csv
        
        aparcmeasures: area volume thicknessstd meancurv gauscurv foldind curvind
        
        hemisphere: lh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-volume.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-lh-curvind.csv
        
        hemisphere: rh
        
        aparcmeasure: area
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-area.csv
        
        aparcmeasure: volume
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-volume.csv
        
        aparcmeasure: thicknessstd
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-thicknessstd.csv
        
        aparcmeasure: meancurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-meancurv.csv
        
        aparcmeasure: gauscurv
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-gauscurv.csv
        
        aparcmeasure: foldind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-foldind.csv
        
        aparcmeasure: curvind
        
        SUBJECTS_DIR : /home/xuser/bitbucket/ceib-cs/freesurfer/test-subjects/
        Parsing the .stats files
        Building the table..
        Writing the table to results-csv/w-g.pct-rh-curvind.csv

**6) check results**

ceib-cs/freesurfer ls

    README.md  results-csv  stats2table.sh  subjects.list  test-subjects

ls results-csv/

    aparc.a2009s-lh-area.csv              aparc-lh-gauscurv.csv      BA-rh-volume.csv                       w-g.pct-lh-curvind.csv
    aparc.a2009s-lh-curvind.csv           aparc-lh-meancurv.csv      BA.thresh-lh-area.csv                  w-g.pct-lh-foldind.csv
    aparc.a2009s-lh-foldind.csv           aparc-lh-thicknessstd.csv  BA.thresh-lh-curvind.csv               w-g.pct-lh-gauscurv.csv
    aparc.a2009s-lh-gauscurv.csv          aparc-lh-thickness.csv     BA.thresh-lh-foldind.csv               w-g.pct-lh-max.csv
    aparc.a2009s-lh-meancurv.csv          aparc-lh-volume.csv        BA.thresh-lh-gauscurv.csv              w-g.pct-lh-meancurv.csv
    aparc.a2009s-lh-thicknessstd.csv      aparc-rh-area.csv          BA.thresh-lh-meancurv.csv              w-g.pct-lh-mean.csv
    aparc.a2009s-lh-thickness.csv         aparc-rh-curvind.csv       BA.thresh-lh-thicknessstd.csv          w-g.pct-lh-nvertices.csv
    aparc.a2009s-lh-volume.csv            aparc-rh-foldind.csv       BA.thresh-lh-thickness.csv             w-g.pct-lh-nvoxels.csv
    aparc.a2009s-rh-area.csv              aparc-rh-gauscurv.csv      BA.thresh-lh-volume.csv                w-g.pct-lh-snr.csv
    aparc.a2009s-rh-curvind.csv           aparc-rh-meancurv.csv      BA.thresh-rh-area.csv                  w-g.pct-lh-std.csv
    aparc.a2009s-rh-foldind.csv           aparc-rh-thicknessstd.csv  BA.thresh-rh-curvind.csv               w-g.pct-lh-thicknessstd.csv
    aparc.a2009s-rh-gauscurv.csv          aparc-rh-thickness.csv     BA.thresh-rh-foldind.csv               w-g.pct-lh-volume.csv
    aparc.a2009s-rh-meancurv.csv          aparc-rh-volume.csv        BA.thresh-rh-gauscurv.csv              w-g.pct-rh-Area_mm2.csv
    aparc.a2009s-rh-thicknessstd.csv      aseg-Area_mm2.csv          BA.thresh-rh-meancurv.csv              w-g.pct-rh-area.csv
    aparc.a2009s-rh-thickness.csv         aseg-max.csv               BA.thresh-rh-thicknessstd.csv          w-g.pct-rh-curvind.csv
    aparc.a2009s-rh-volume.csv            aseg-mean.csv              BA.thresh-rh-thickness.csv             w-g.pct-rh-foldind.csv
    aparc.DKTatlas40-lh-area.csv          aseg-nvertices.csv         BA.thresh-rh-volume.csv                w-g.pct-rh-gauscurv.csv
    aparc.DKTatlas40-lh-curvind.csv       aseg-nvoxels.csv           entorhinal_exvivo-lh-area.csv          w-g.pct-rh-max.csv
    aparc.DKTatlas40-lh-foldind.csv       aseg-std.csv               entorhinal_exvivo-lh-curvind.csv       w-g.pct-rh-meancurv.csv
    aparc.DKTatlas40-lh-gauscurv.csv      aseg-volume.csv            entorhinal_exvivo-lh-foldind.csv       w-g.pct-rh-mean.csv
    aparc.DKTatlas40-lh-meancurv.csv      BA-lh-area.csv             entorhinal_exvivo-lh-gauscurv.csv      w-g.pct-rh-nvertices.csv
    aparc.DKTatlas40-lh-thicknessstd.csv  BA-lh-curvind.csv          entorhinal_exvivo-lh-meancurv.csv      w-g.pct-rh-nvoxels.csv
    aparc.DKTatlas40-lh-thickness.csv     BA-lh-foldind.csv          entorhinal_exvivo-lh-thicknessstd.csv  w-g.pct-rh-snr.csv
    aparc.DKTatlas40-lh-volume.csv        BA-lh-gauscurv.csv         entorhinal_exvivo-lh-thickness.csv     w-g.pct-rh-std.csv
    aparc.DKTatlas40-rh-area.csv          BA-lh-meancurv.csv         entorhinal_exvivo-lh-volume.csv        w-g.pct-rh-thicknessstd.csv
    aparc.DKTatlas40-rh-curvind.csv       BA-lh-thicknessstd.csv     entorhinal_exvivo-rh-area.csv          w-g.pct-rh-volume.csv
    aparc.DKTatlas40-rh-foldind.csv       BA-lh-thickness.csv        entorhinal_exvivo-rh-curvind.csv       wmparc-Area_mm2.csv
    aparc.DKTatlas40-rh-gauscurv.csv      BA-lh-volume.csv           entorhinal_exvivo-rh-foldind.csv       wmparc-max.csv
    aparc.DKTatlas40-rh-meancurv.csv      BA-rh-area.csv             entorhinal_exvivo-rh-gauscurv.csv      wmparc-mean.csv
    aparc.DKTatlas40-rh-thicknessstd.csv  BA-rh-curvind.csv          entorhinal_exvivo-rh-meancurv.csv      wmparc-nvertices.csv
    aparc.DKTatlas40-rh-thickness.csv     BA-rh-foldind.csv          entorhinal_exvivo-rh-thicknessstd.csv  wmparc-nvoxels.csv
    aparc.DKTatlas40-rh-volume.csv        BA-rh-gauscurv.csv         entorhinal_exvivo-rh-thickness.csv     wmparc-std.csv
    aparc-lh-area.csv                     BA-rh-meancurv.csv         entorhinal_exvivo-rh-volume.csv        wmparc-volume.csv
    aparc-lh-curvind.csv                  BA-rh-thicknessstd.csv     w-g.pct-lh-Area_mm2.csv
    aparc-lh-foldind.csv                  BA-rh-thickness.csv        w-g.pct-lh-area.csv
