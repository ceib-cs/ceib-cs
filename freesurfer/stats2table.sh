#!/bin/bash

: '
Subdirección General de Sistemas de Información para la Salud

Centro de Excelencia e Innovación Tecnológica de Bioimagen de la Conselleria de Sanitat

http://ceib.san.gva.es/

María de la Iglesia Vayá

Ángel Fernández-Cañada Vilata

Jorge Isnardo Altamirano

---

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/gpl-3.0.txt>.
'

# $1 = SUBJECTS_DIR
# $2 = OS        : linux / mac
# $3 = delimiter : csv   / tsv

if test $# -ne 3; then
    
    echo -e "\nexport SUBJECTS_DIR=/..."
    
    echo -e '\nsource $FREESURFER_HOME/SetUpFreeSurfer.sh'
    
    echo -e "\nbash $0 \$SUBJECTS_DIR OS delimiter" # Use bash to avoid syntax error: "(" unexpected
    
    echo -e "\nOS        : linux / mac"
    echo -e "delimiter : csv   / tsv\n"
    
    exit 1
    
else
    
    # ---------------
    # asegstats2table
    # ---------------
    #
    # Default : --common-segs
    #
    # --------------------------------------------------------------------------------
    #
    # ERROR: All stat files should have the same segmentations
    #        If one or more stats file have different segs from others,
    #        use --common-segs or --all-segs flag depending on the need.
    #
    # --common-segs : output only the common segmentations of all the statsfiles given
    # --all-segs    : output all the segmentations of the statsfiles given
    #
    # --------------------------------------------------------------------------------
    
    os=""
    delimiter=""
    
    if [ "$2" == "linux" ] || [ "$2" == "mac" ] && [ "$3" == "csv" ] || [ "$3" == "tsv" ]; then
        
        os=$2
        delimiter=$3
        
    else
        
        echo -e "\nunexpected\n\nos (linux / mac)      : $2\ndelimiter (csv / tsv) : $3\n"
        
        exit 1
        
    fi
    
    # -------------
    # subjects.list
    # -------------
    
    output="results-$delimiter"
    
    rm -rf $output/ && mkdir $output
    
    subjectsfile="subjects.list"
    
    rm -f $subjectsfile
    
    for subject in `ls -l $1/ | grep ^d | awk {'print $9'}`; do
        echo $subject >> $subjectsfile
    done
    
    echo -e "\nSubjects\n"
    
    cat $subjectsfile
    
    # ----------
    # statsfiles
    # ----------
    
    # aseg
    # wmparc
    
    # ColHeaders : Index SegId NVoxels Volume_mm3 StructName normMean normStdDev normMin normMax normRange
    
    statsfiles=(aseg wmparc)
    
    echo -e "\nstatsfiles: ${statsfiles[@]}"
    
    # ------------
    # asegmeasures
    # ------------
    
    asegmeasures=(volume Area_mm2 nvoxels nvertices mean std max)
    
    echo -e "\nasegmeasures: ${asegmeasures[@]}"
    
    # -------------------
    # save with delimiter
    # -------------------
    
    for statsfile in "${statsfiles[@]}"; do
        
        echo -e "\nstatsfile: $statsfile"
        
        for asegmeasure in "${asegmeasures[@]}"; do
            
            echo -e "\nasegmeasure: $asegmeasure\n"
            
            file="$statsfile-$asegmeasure.$delimiter"
            
            # data dump
            
            if [ "$delimiter" == "csv" ]; then
                
                asegstats2table --subjectsfile $subjectsfile -d comma --common-segs --stats $statsfile.stats -m $asegmeasure -t $output/$file
                
            else # tsv
                
                asegstats2table --subjectsfile $subjectsfile --common-segs --stats $statsfile.stats -m $asegmeasure -t $output/$file
                
            fi
            
            # fixx col 1 name
            
            if [ "$os" == "linux" ]; then
                
                sed -i "s/Measure:$asegmeasure/Subject/g" $output/$file
                
            else # mac
                
                sed -i '' "s/Measure:$asegmeasure/Subject/g" $output/$file
                
            fi
        
        done
        
    done
    
    # ----------
    # statsfiles
    # ----------
    
    # aparc.a2009s
    # aparc.DKTatlas40
    # aparc
    # BA
    # BA.thresh
    # entorhinal_exvivo
    
    # ColHeaders -> StructName NumVert SurfArea GrayVol ThickAvg ThickStd MeanCurv GausCurv FoldInd CurvInd
    
    statsfiles=(aparc.a2009s aparc.DKTatlas40 aparc BA BA.thresh entorhinal_exvivo)
    
    echo -e "\nstatsfiles: ${statsfiles[@]}"
    
    # -----------
    # hemispheres
    # -----------
    
    hemispheres=(lh rh)
    
    echo -e "\nhemispheres: ${hemispheres[@]}"
    
    # -------------
    # aparcmeasures
    # -------------
    
    aparcmeasures=(area volume thickness thicknessstd meancurv gauscurv foldind curvind)
    
    echo -e "\naparcmeasures: ${aparcmeasures[@]}"
    
    # -------------------
    # save with delimiter
    # -------------------
    
    for statsfile in "${statsfiles[@]}"; do
        
        echo -e "\nstatsfile: $statsfile"
        
        for hemisphere in "${hemispheres[@]}"; do
            
            echo -e "\nhemisphere: $hemisphere"
            
            for aparcmeasure in "${aparcmeasures[@]}"; do
                
                echo -e "\naparcmeasure: $aparcmeasure\n"
                
                file="$statsfile-$hemisphere-$aparcmeasure.$delimiter"
                
                # data dump
                
                if [ "$delimiter" == "csv" ]; then
                    
                    aparcstats2table --subjectsfile $subjectsfile -d comma --hemi $hemisphere --parc $statsfile -m $aparcmeasure -t $output/$file
                    
                else # tsv
                    
                    aparcstats2table --subjectsfile $subjectsfile --hemi $hemisphere --parc $statsfile -m $aparcmeasure -t $output/$file
                    
                fi
                
                # Fix col 1 name
                
                if [ "$os" == "linux" ]; then
                    
                    sed -i "s/$hemisphere.$statsfile.$aparcmeasure/Subject/g" $output/$file
                    
                else # mac
                    
                    sed -i '' "s/$hemisphere.$statsfile.$aparcmeasure/Subject/g" $output/$file
                    
                fi
                
            done
            
        done
        
    done
    
    # ---------
    # statsfile
    # ---------
    
    # w-g.pct
    
    # ColHeaders -> Index SegId NVertices Area_mm2 StructName Mean StdDev Min Max Range SNR
    
    statsfile="w-g.pct"
    
    echo -e "\nstatsfile: $statsfile"
    
    # ------------
    # asegmeasures
    # ------------
    
    # append snr
    
    asegmeasures+=(snr)
    
    echo -e "\nasegmeasures: ${asegmeasures[@]}"
    
    # -------------------
    # save with delimiter
    # -------------------
    
    for hemisphere in "${hemispheres[@]}"; do
        
        for asegmeasure in "${asegmeasures[@]}"; do
            
            echo -e "\nasegmeasure: $asegmeasure\n"
            
            file="$statsfile-$hemisphere-$asegmeasure.$delimiter"
            
            # data dump
            
            if [ "$delimiter" == "csv" ]; then
                
                asegstats2table --subjectsfile $subjectsfile -d comma --common-segs --stats $hemisphere.$statsfile.stats -m $asegmeasure -t $output/$file
                
            else # tsv
                
                asegstats2table --subjectsfile $subjectsfile --common-segs --stats $hemisphere.$statsfile.stats -m $asegmeasure -t $output/$file
                
            fi
            
            # Fix col 1 name
            
            if [ "$os" == "linux" ]; then
                
                sed -i "s/Measure:$asegmeasure/Subject/g" $output/$file
                
            else # mac
                
                sed -i '' "s/Measure:$asegmeasure/Subject/g" $output/$file
                
            fi
            
        done
        
    done
    
    # -------------
    # aparcmeasures
    # -------------
    
    # pop thickness
    
    aparcmeasures=("${aparcmeasures[0]}" "${aparcmeasures[1]}" "${aparcmeasures[3]}" "${aparcmeasures[4]}" "${aparcmeasures[5]}" "${aparcmeasures[6]}" "${aparcmeasures[7]}")
    
    echo -e "\naparcmeasures: ${aparcmeasures[@]}"
    
    # -------------------
    # save with delimiter
    # -------------------
    
    for hemisphere in "${hemispheres[@]}"; do
        
        echo -e "\nhemisphere: $hemisphere"
        
        for aparcmeasure in "${aparcmeasures[@]}"; do
            
            echo -e "\naparcmeasure: $aparcmeasure\n"
            
            file="$statsfile-$hemisphere-$aparcmeasure.$delimiter"
            
            # data dump
            
            if [ "$delimiter" == "csv" ]; then
                
                aparcstats2table --subjectsfile $subjectsfile -d comma --hemi $hemisphere --parc $statsfile -m $aparcmeasure -t $output/$file
                
            else # tsv
                
                aparcstats2table --subjectsfile $subjectsfile --hemi $hemisphere --parc $statsfile -m $aparcmeasure -t $output/$file
                
            fi
            
            # fix col 1 name
            
            if [ "$os" == "linux" ]; then
                
                sed -i "s/$hemisphere.$statsfile.$aparcmeasure/Subject/g" $output/$file
                
            else # mac
                
                sed -i '' "s/$hemisphere.$statsfile.$aparcmeasure/Subject/g" $output/$file
                
            fi
            
        done
        
    done
    
    # ---------
    # statsfile
    # ---------
    
    # curv
    
    exit 0
fi
